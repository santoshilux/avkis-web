
import os
import shelve
import socket
import sys
import pandas as pd

basedir = os.path.abspath(os.path.dirname(__file__))
#print("basedir info: ", basedir)

webpack = "HOH"
#webpack = "AVANDIL"

class Config:
    hostname = socket.gethostname()
    IP = socket.gethostbyname(hostname)
    print("IP-Adresse: ", IP)

    DEBUG = True

    SHELF_FILE_NAME = ""
    DATABASE = ""
    RECORDS = ""
    DBDIR = ""
    KI_ERGEBNIS = pd.DataFrame
    B_LAND = {}
    STOPWORDS = []
    TFIDF_WZ = None
    TFIDF_TT = None
    RAW_DATA_TFIDF = None
    IS_ADMIN = False
    RAWDATDF = basedir+"\\config\\rawdat.df"
    WZPKL = basedir + "\\config\\wz.pkl"
    TTPKL = basedir + "\\config\\tt.pkl"
    op_system = sys.platform
    if op_system == "win32" :
        environment = "Win"
    if op_system == "darwin" :
        environment = "Mac"

    if environment == "Win" :
        SHELF_FILE_NAME = "avkis_Win"
        backslash = "\\"
        SHELF_FILE = shelve.open(SHELF_FILE_NAME)

    if environment == "Mac" :
        SHELF_FILE_NAME = "avkis_Mac"
        backslash = "/"
        SHELF_FILE = shelve.open(SHELF_FILE_NAME)

    if "DBDIR" in SHELF_FILE :
        DBDIR = SHELF_FILE["DBDIR"]
    else :
        DBDir = basedir
        if webpack == "HOH" :
            DBDIR = "C:\\Lokale Dateien"
        SHELF_FILE["DBDIR"] = DBDIR
    if "DATABASE" in SHELF_FILE :
        DATABASE = SHELF_FILE["DATABASE"]
    else :
        DATABASE = basedir + backslash + "files" + backslash + "Firmen.db"
        SHELF_FILE["DATABASE"] = DATABASE

    if "STOPWORDS" in SHELF_FILE :
        STOPWORDS = SHELF_FILE["STOPWORDS"]
    # SHELF_FILE['DBDIR'] = "C:\\Lokale Daten\\AVKIS-Web\\database"
    # SHELF_FILE['DBDIR'] = "C:\\Lokale Daten\\AVKIS-Web\\database\\Firmen.db"
    SHELF_FILE['DBDIR'] = "C:\\Users\\Sandeep Agrawal\\Dropbox\\AVKIS-Web\\database"
    SHELF_FILE['DBDIR'] = "C:\\Users\\Sandeep Agrawal\\Dropbox\\AVKIS-Web\\database\\Firmen.db"
    DATABASE = ""
    SHELF_FILE.sync()
    SHELF_FILE.close()
    
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
#    FLASKY_ADMIN = os.environ.get('FLASKY_ADMIN')

#    RECORDS = tfidf.query.all().count()



    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(Config.DBDIR, Config.DATABASE)


class TestingConfig(Config):
    TESTING = True
#    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
#        'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')


class ProductionConfig(Config):
#    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
#        'sqlite:///' + os.path.join(basedir, 'data.sqlite')
    pass

config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': DevelopmentConfig
}
