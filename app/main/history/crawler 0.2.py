"""Created on Sun Jan 29 15:15:52 2017

Einfache Klasse zum Crawlen von Websites für das AvCrawl-Projekt
Übergeben wird eine URL die zu crawlen ist und die Klasse speichert dann den Text der Website

@author: Holger
"""

from bs4 import BeautifulSoup
from urllib.error import HTTPError
from urllib.parse import urlparse

import pandas as pd
import requests

"""
Funktion: verify_url(url)
Zweck: prüft die übergebene url und gibt eine url zurück, die direkt mit html.open zu öffnen ist
"""
#def verify_url(url):
#    test = urlparse(url)
#    if not test.scheme :
#        scheme = "http:"
#    else :
#        scheme = test.scheme
#        if not scheme.endswith(":") :
#          scheme = scheme + ":"
#    if not test.netloc :
#         netloc = test.path
#         path = ""
#    else :
#         netloc = test.netloc
#    if test.path != netloc:
#         path = test.path
#    url = scheme + "//" + netloc + "/" + path
#    return scheme, url

"""--------
Funktion: get_object
--------"""
def get_object(url):
    try :
#        print("URL: " + url)
        try :
            r = requests.get(url)
#            html = r.content.decode('utf-8', 'ignore')
            html = r.content.decode('latin1', 'ignore')
        except :
            ReadError = None        
            return ReadError
    except HTTPError as ReadError:
        ReadError = None        
        return ReadError

    try :
        soup = BeautifulSoup(html, "lxml")
    except AttributeError as ReadError:
        ReadError = None        
        return ReadError
    return soup

"""--------
Klassendefinition: Crawler
--------"""
class Crawler:
    def __init__(self, url = "http://avandil.com"):
        self.url = url
        self.pages = []
        self.log_daten = []
        self.no_search_filetypes = [".pdf", ".jpg", ".png", ".swf", ".zip", ".jpeg", ".mp3", ".mp4", ".wmv", ".mov", ".epub", "@full"
                    , "?pdf=1", ".exe", "bat"]
        self.no_search_sites = ["sitemap", "impressum", "kontakt", "download", "standort", "agb", "datenschutz", "login", "site-map", 
                           "contact", "youtube", "google", "anfahrt", "karriere", "location", "printpdf"]
        self.seiteninhalt = ""
        self.encode = "latin1"
        self.page_content = []

                    
    def crawl_site(self):
        url = self.url
        scheme, start_url = self.verify_main_url(url)
        self.pages = [start_url]
        for page in self.pages : # self.pages wird rekursiv über get_links und verify_sub_link gefüllt
            newpages = self.get_links(scheme, page)
            for page in newpages :
                if page not in self.pages :
                    self.pages.append(page)
        print(self.pages)
#        self.read_pages()
#        print("======= ERGEBNIS ========")
#        if self.seiteninhalt :
#            print(self.seiteninhalt)
#            return True
#        else :
#            print(self.log)
#            return False
    
    def get_links(self, scheme, url):
        pages = []
        try :
            req = requests.get(url)
        except requests.exceptions.Timeout:
            pass
            self.log_daten.append("Timeout")
            return False
        except :
            self.log_daten.append("Fehler bei der Abfrage")
            return
        html = req.content.decode(self.encode, 'ignore')
        try :
            url = html.geturl() #Für den Fall redirecting der url
        except: 
            pass

        domain = urlparse(url).netloc
        log_text = "Website crawlen: " + domain + "\n"
        self.log_daten.append(log_text)
        bsObj = BeautifulSoup(html, "lxml")
        if len(self.pages) == 0 and len(bsObj.findAll("a")) == 0:
            self.log.append("Keine weiterführenden Links gefunden")
            return False
        for link in bsObj.findAll("a") :
            linktext = link.get('href')
            if not linktext :
                continue
            parse_not = False     
            for site in self.no_search_sites :
                if linktext.lower().find(site) > 0 :
                    parse_not = True
                    break
            if parse_not :
                continue
            newpage = self.verify_sub_url(scheme, domain, linktext)
            if newpage and (newpage not in pages):
                pages.append(newpage)
                self.page_content.append((newpage, bsObj))
        if len(pages) == 0 :
            self.log_daten.append("Keine weiterführenden Links gefunden")
            return None
        else :
            return pages                          
                        
    def verify_sub_url(self, scheme, domain, url):
        urlpage = urlparse(url)
        if urlpage.scheme and not urlpage.scheme.startswith("http") :
            return None
        if urlpage.netloc and urlpage.netloc != domain :
            nl = urlpage.netloc.replace("www.","",1)
            dm = domain.replace("www.","",1)
            if nl != dm :
                return None
        path = urlpage.path
        parse_not = False
        for filetype in self.no_search_filetypes :
            if path and path.lower().endswith(filetype) :
                parse_not = True
                break
        if parse_not :
            return None
        if path and path.startswith("/") :
            path = path.replace("/","",1)
        if path and path.startswith("../") :
            path = path.replace("../","",1)
        if path and path.startswith("./") : #wenn relativer Pfad angegeben wird
            urlpath = urlparse(url).path
            urlpathlist = urlpath.split("/")
            path_location = urlpathlist[:len(urlpathlist)-1]
            pathlist = path.split("/")
            location = pathlist[1:]
            newpath = ""
            for part in path_location :
                if part :
                    if newpath :
                        newpath = newpath + "/" + part
                    else :
                        newpath = part
            newpath = newpath + "/" + location[0]
            path = newpath
        query = urlpage.query
        if query :
            query = "?" + query
              
        newpage = scheme + "//" + domain + "/" + path + query
        self.log_daten.append("Gefunden: " + newpage)
        return newpage

    def verify_main_url(self, url):
        test = urlparse(url)
        if not test.scheme :
            scheme = "http:"
        else :
            scheme = test.scheme
            if not scheme.endswith(":") :
              scheme = scheme + ":"
        if not test.netloc :
             netloc = test.path
             path = ""
        else :
             netloc = test.netloc
        if test.path != netloc:
             path = test.path
        url = scheme + "//" + netloc + "/" + path
        return scheme, url


    def read_pages(self) :
        page_refs = ["title", "h1", "h2", "h3", "h4", "h5", "p", "li", "div"]
        textliste = []
        pages = self.pages
        for url in pages:
            try :
                req = requests.get(url)
                url2 = req.url #Für den Fall redirecting der url
                if url != url2 :
                    print("Redirect: ", url2)
                    url = url2
            except: 
                pass
            parse_not = False
            for filetype in self.no_search_filetypes :
                if url.lower().endswith(filetype) :
                    parse_not = True
            if parse_not :
                continue
            soup = get_object(url)
            if soup == None:
                print("Object not found")
                self.log.append("Object not found" + url)
            else:
                for ref in page_refs :
                    seite = soup.findAll(ref)
                    for zeile in seite :
                        wort = zeile.get_text()
#                        text = ":: " + wort
#                        print(text, "\n")
                        textliste.append(wort)

        pagetext = clean_page_text(textliste)                    
        taetigkeit = ""
        for wort in pagetext :
            taetigkeit = taetigkeit + " " + wort
        self.seiteninhalt = taetigkeit

#    def __init__(self):
#        self.Url = "http://avandil.com"
#        
#        self.pages = set()
#        self.Textliste = pd.DataFrame()
#        self.Stopliste = pd.DataFrame()
#        self.Order = "Anzahl"
#        self.StopWords = set()
#        self.Tabelle = "Stopwords"
#        self.ShowList = "Suchwörter"
#        self.Zaehler = True
#        self.AID = "5210308117" # AVANDIL
#        self.no_search_filetypes = [".pdf", ".jpg", ".png", ".swf", ".zip", ".jpeg", ".mp3", ".mp4", ".wmv", ".mov", ".epub", "@full"
#                    , "?pdf=1", ".exe", "bat"]

#
#    def start_crawling(self,display) :
#        self.get_links(display)
#        if not self.pages :
#            display.insert("end", "Crawlingprozess beendet - Keine zu crawlenden Seiten gefunden\n\n")
#            display.update_idletasks
#            errlog = self.AID + ", " + self.Url[0]
#            ErrlogSchreiben(errlog)                
#            return                    
#        self.read_pages(display)
#
#    def show(self, display) :
#        if self.ShowList == "Suchwörter" :
#            liste = self.Textliste
#        elif self.ShowList == "Stopwörter" :
#            liste = self.Stopliste
#        display.delete(1.0, END)
#        if len(liste) :
#            for index,row in liste.iterrows() :
#                if self.Zaehler :
#                    zeile = row["Wort"] + " : " + str(row["Anzahl"]) + "\n"
#                else :
#                    zeile = row["Wort"] + "\n"
#                display.insert(END,zeile)
#        else :
#                display.insert(END,"Keine Wörter vorhanden\n")


print("Crawler gestartet")
KiCrawl = Crawler("www.bus-taxi-sieben.de")
#KiCrawl.url = "www.bus-taxi-sieben.de"
ok = KiCrawl.crawl_site()
print("Ende des Programms")
    

