from flask import render_template, session, redirect, url_for, current_app
from .. import db
from ..models import tfidf
from . import main
from .forms import NameForm
from .import user_interface as ui
from openpyxl.utils import get_column_letter

import openpyxl
import pandas as pd
import pdb
#Debug:  pdb.set_trace()

# Später evtl. wieder wg
from tkinter import *
from tkinter import filedialog


@main.route('/') #, methods=['GET', 'POST'])
def index():
#    dbfile = globals["dbfile"]
#    records = globals["records"]
    dbfile = current_app.config["DATABASE"]
    rec_count = tfidf.query.count()
    records = format(rec_count,",").replace(",",".")
    return render_template('index.html', datenbank=dbfile, records=records)

@main.route('/Datenbank_Oeffnen/')
def datenbank_oeffnen():
    dbfile = ui.open_file()
    shelf_file = current_app.config["SHELF_FILE"]
    shelf_file["DATABASE"] = dbfile
    shelf_file.sync()
    rec_count = ""
    return render_template('index.html', datenbank=dbfile, records=rec_count)

@main.route('/Datenbank_Zeigen/')
def datenbank_zeigen():
    liste = db.session.query(tfidf.AID, tfidf.Name, tfidf.Tätigkeit)
    return render_template('show_database.html', items=liste)

@main.route('/KI_Suche/')
def ki_suche():
    aid = ui.MyInput("AID eingeben", "AID:")
    if aid :
        current_app.config["AID"] = aid
        ki_ergebnis = ui.create_tfidf(aid)
        current_app.config["KI-Ergebnis"] = ki_ergebnis
        return render_template('ki_ergebnis.html', items=ki_ergebnis)
    else :
        dbfile = current_app.config["DATABASE"]
        rec_count = tfidf.query.count()
        return render_template('index.html', datenbank=dbfile, records=rec_count)

@main.route('/KI_Export/')
def ki_export():
    ExportFile = ui.open_file()
    if ExportFile :
        writer = pd.ExcelWriter(ExportFile)
        df1 = current_app.config["KI-Ergebnis"]
        df1.to_excel(writer, sheet_name='KI-Suche', index=False)
        writer.save()
        Text = "Excel-Datei erstellt"
        ui.MyMsgBox("Ergebnis", Text) 
    dbfile = current_app.config["DATABASE"]
    rec_count = tfidf.query.count()
    return render_template('index.html', datenbank=dbfile, records=rec_count)

@main.route('/KI_Check/')
def liste_pruefen() :
    dbfile = current_app.config["DATABASE"]
    rec_count = tfidf.query.count()
    root = Tk()
    root.withdraw()
    options = {}
    options['defaultextension'] = '.xlsx'
    options['filetypes'] = [('Excel files', '.xlsx'), ('All files', '.*')]
    options['initialdir'] = current_app.config["DBDIR"]
    options['initialfile'] = 'KI-Suche.xlsx'
    options['parent'] = root
    options['title'] = 'Datei wählen'
    ExcelFile = filedialog.askopenfilename(**options)
    root.destroy()
    if not ExcelFile :
        return render_template('index.html', datenbank=dbfile, records=rec_count)
    wb = openpyxl.load_workbook(ExcelFile)
    try:
        sheet = wb.get_sheet_by_name("KI-Suche")
    except :
        Text = 'Die Datei\n\n' + ExcelFile + '\n\nenthält kein Sheet mit dem Namen "KI-Suche" !\nVorgang wird abgebrochen.'
        ui.MyMsgBox("Excel: Dateifehler", Text) 
        return render_template('index.html', datenbank=dbfile, records=rec_count)
    ueberschrift = sheet["A1"].value
    if ueberschrift != "AID" :
        Text = 'Das Excel-Sheet "KI-Suche" enthält keine Überschrift "AID"" !\nVorgang wird abgebrochen.'
        ui.MsgBox("Excel: Dateifehler", Text) 
        return render_template('index.html', datenbank=dbfile, records=rec_count)
    ki_liste = []
    anzahl = sheet.max_row + 1
    for row in range(2, anzahl):
        aid = sheet["A" + str(row)].value
        ki_liste.append(aid)

    ki_ergebnis = ui.create_tfidf(ki_liste[0])
#    ki_ergebnis = tfidf(globals["dbfile"],globals["connection"], globals["cursor"], ki_liste[0], ki_liste)
    current_app.config["KI-Ergebnis"] = ki_ergebnis
    return render_template('ki_ergebnis.html', items=ki_ergebnis)

@main.route('/Test/')
def test() :
    return render_template('pop_up.html')

    

#@main.route('/', methods=['GET', 'POST'])
#def index():
#    form = NameForm()
#    if form.validate_on_submit():
#        user = User.query.filter_by(username=form.name.data).first()
#        if user is None:
#            user = User(username=form.name.data)
#            db.session.add(user)
#            session['known'] = False
#            if current_app.config['FLASKY_ADMIN']:
#                send_email(current_app.config['FLASKY_ADMIN'], 'New User',
#                           'mail/new_user', user=user)
#        else:
#            session['known'] = True
#        session['name'] = form.name.data
#        return redirect(url_for('.index'))
#    return render_template('index.html',
#                           form=form, name=session.get('name'),
#                           known=session.get('known', False))
#