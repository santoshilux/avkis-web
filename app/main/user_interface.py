# -*- coding: utf-8 -*-

from .. import db
from ..models import tfidf
from . import main

from flask import current_app
from pandas import Series, DataFrame, ExcelWriter
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from tkinter import *
from tkinter import filedialog
from tkinter import simpledialog
from tkinter import messagebox


import numpy as np
import os
import pandas as pd
import pdb
import pickle




"""---------------------------------------------------
Funktion:   open_file
Zweck:      fildedialog-Implementation für Web-Anwendung
-----------------------------------------------------"""
def open_file():
    root = Tk()
    root.withdraw()
    options = {}
    options['defaultextension'] = '.db'
    options['filetypes'] = [('db files', '.db'), ('All files', '.*')]
    options['initialdir'] = current_app.config["DBDIR"]
    options['initialfile'] = 'Firmen.db'
    options['parent'] = root
    options['title'] = 'Datenbank wählen'
    filename = filedialog.askopenfilename(**options)
    root.destroy()
    return(filename)

"""---------------------------------------------------
Funktion:   save_file
Zweck:      fildedialog-Implementation für Web-Anwendung
-----------------------------------------------------"""
def save_file():
    root = Tk()
    root.withdraw()
    options = {}
    options['defaultextension'] = '.xlsx'
    options['filetypes'] = [('Excel files', '.xlsx'), ('All files', '.*')]
    options['initialdir'] = current_app.config["DBDIR"]
    options['initialfile'] = 'AID.xlsx'
    options['parent'] = root
    options['title'] = 'Datei wählen'
    filename = filedialog.asksaveasfilename(**options)
    root.destroy()
    return(filename)
    
"""--------
Funktion: MyInput
--------"""
def MyInput(text, msg):
    root = Tk()
    root.withdraw()
    aid = simpledialog.askstring(text, msg)
    root.destroy()
    return aid

"""--------
Funktion: MyMsgBox
--------"""
def MyMsgBox(headline, msg):
    root = Tk()
    root.withdraw()
    messagebox.showinfo(headline, msg) 
    root.destroy()

    
"""--------
Funktion: tfidf
--------"""
def create_tfidf(Company, selektion=[], suchworte=""):
    file_name = current_app.config['RAWDATDF']
    if current_app.config["RAW_DATA_TFIDF"] is not None:
        raw_data = current_app.config["RAW_DATA_TFIDF"].copy()
    elif os.path.exists(file_name):
        current_app.config["RAW_DATA_TFIDF"] = pd.read_pickle(file_name)
        raw_data = current_app.config["RAW_DATA_TFIDF"].copy()
    else:
        current_app.config["RAW_DATA_TFIDF"] = pd.read_sql(tfidf.query.statement, db.session.bind)
        raw_data = current_app.config["RAW_DATA_TFIDF"].copy()
        raw_data.to_pickle(file_name)

    # Standard: Einfache Suche oder allererster Durchlauf -> erzeugt tfidf

    tfidf_vectorizer = TfidfVectorizer()
    file_name = current_app.config['WZPKL']
    WZ_Liste = raw_data["BCH"]
    if current_app.config["TFIDF_WZ"] is not None:
        tfidf_wz = current_app.config["TFIDF_WZ"]
    elif os.path.exists(file_name) :
        current_app.config["TFIDF_WZ"] = pickle.load(open(file_name, "rb"))
        tfidf_wz = current_app.config["TFIDF_WZ"]
    else :
        current_app.config["TFIDF_WZ"] = tfidf_vectorizer.fit_transform(WZ_Liste)
        tfidf_wz = current_app.config["TFIDF_WZ"]
        pickle.dump(tfidf_wz, open(file_name, "wb"))
    file_name = current_app.config['TTPKL']
    TT_Liste = raw_data["Tätigkeit"] + raw_data["Name"]
    if current_app.config["TFIDF_TT"] is not None:
        tfidf_tt = current_app.config["TFIDF_TT"]
    elif os.path.exists(file_name) :
        current_app.config["TFIDF_TT"] = pickle.load(open(file_name, "rb"))
        tfidf_tt = current_app.config["TFIDF_TT"]
    else :
        current_app.config["TFIDF_TT"] = tfidf_vectorizer.fit_transform(TT_Liste)
        tfidf_tt = current_app.config["TFIDF_TT"]
        pickle.dump(tfidf_tt, open(file_name, "wb"))

    if Company != "0000000000" :
        try:
            Nummer = raw_data.loc[raw_data["AID"] == Company].index[0]
        except:
            return "no_aid"
    else :
        Nummer = len(raw_data)

    # suchworte vorhanden -> müssen in die Tätigkeitsbeschreibung hinzugefügt werden
    if suchworte != "" :
        if Nummer == len(raw_data) :
            taetigkeit = suchworte
            raw_data.loc[Nummer] = [Company, "", taetigkeit, "XX", "XX", "XX"]
        else :
            taetigkeit = suchworte + raw_data.loc[Nummer, "Tätigkeit"]
            raw_data.loc[Nummer, "Tätigkeit"] = taetigkeit
        WZ_Liste = raw_data["BCH"]
        tfidf_wz = tfidf_vectorizer.fit_transform(WZ_Liste)
        TT_Liste = raw_data["Tätigkeit"] + raw_data["Name"]
        tfidf_tt = tfidf_vectorizer.fit_transform(TT_Liste)

    # Bewertung der WZ
    WZ_Gewichtung = 2
    companies = raw_data["AID"]
    ar = cosine_similarity(tfidf_wz[Nummer:Nummer+1], tfidf_wz)
    Ergebnisliste = ar[0]
    data = {"Company" : companies, "Wert" : Ergebnisliste}
    frame_WZ = DataFrame(data)
    frame_WZ["Wert"] = frame_WZ["Wert"] * WZ_Gewichtung

    # Bewertung der Tätigkeitsbeschreibung
    ar = cosine_similarity(tfidf_tt[Nummer:Nummer+1], tfidf_tt)
    Ergebnisliste = ar[0]
    data = {"Company" : companies, "Wert" : Ergebnisliste}
    frame_Ttg = DataFrame(data)

    frame_final = frame_WZ
    frame_final["Wert"] = round(frame_WZ["Wert"] + frame_Ttg["Wert"], 3)
    frame_final["Name"] = raw_data["Name"]
    frame_final["Tätigkeit"] = raw_data["Tätigkeit"]
    
    result_frame = frame_final.sort_values(by="Wert", ascending = False)
    if not selektion :
        result_frame = result_frame[result_frame.Wert > 0.1]
        result_frame = result_frame[result_frame.Wert != 2.0]
    else :
        result_frame = result_frame[result_frame["Company"].isin(selektion)]

    result_frame.reset_index(drop=True, inplace=True)
    return(result_frame)

