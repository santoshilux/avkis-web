"""Created on Sun Jan 29 15:15:52 2017

Einfache Klasse zum Crawlen von Websites für das AvCrawl-Projekt
Übergeben wird eine URL die zu crawlen ist und die Klasse speichert dann den Text der Website

@author: Holger
"""

from bs4 import BeautifulSoup
from nltk import FreqDist
from urllib.error import HTTPError
from urllib.parse import urlparse

import nltk, pdb
import pandas as pd
import re
import requests

"""--------
Klassendefinition: Crawler
--------"""
class Crawler:
    def __init__(self, url = "http://avandil.com"):
        self.url = url
        self.pages = []
        self.log_daten = []
        self.no_search_filetypes = [".pdf", ".jpg", ".png", ".swf", ".zip", ".jpeg", ".mp3", ".mp4", ".wmv", ".mov", ".epub", "@full"
                    , "?pdf=1", ".exe", "bat"]
        self.no_search_sites = ["sitemap", "impressum", "kontakt", "download", "standort", "agb", "datenschutz", "login", "site-map", 
                           "contact", "youtube", "google", "anfahrt", "karriere", "location", "printpdf", "anfrage", "job", "jobs"]
        self.seiteninhalt = ""
        self.encode = "latin1"
        self.page_content = []
        self.page_text = []
        self.ngrams = pd.DataFrame
        self.stopwords = []

                    
    def crawl_site(self):
        url = self.url
        scheme, start_url = self.verify_main_url(url)
        url, content = self.get_object(start_url)
        self.pages = [url]
        self.page_content = [(url, content)]
        for page in self.pages : # self.pages wird rekursiv über get_links und verify_sub_link gefüllt
            #pdb.set_trace()
            newpages, page_content = self.get_links(scheme, page)
            pc_dict = dict(page_content)
            for new_page in newpages :
#                if page == "http://www.meierguss.de/mobile/index.html":
#                    print ("Waiting")
#                 print("Page: ", new_page, "iiiiiiii-", i)
#                 print(len(self.pages))
#                 print(len(list(set(self.pages))))
                if new_page not in self.pages :
                    try :
                        self.pages.append(new_page)
                        content = pc_dict[new_page]
                        self.page_content.append((new_page, content))
                    except:
                        pass
#        print(self.pages)
        self.page_text = self.get_page_text()
        self.ngrams = self.suchwort_extractor()
        return True
    
    def get_object(self, url):
        for filetype in self.no_search_filetypes :
            if url and url.lower().endswith(filetype) :
                return None, None
        #pdb.set_trace()
        try :
            try :
                req = requests.get(url)
            except requests.exceptions.Timeout:
                self.log_daten.append("Timeout")
                return None, None
            except :
                ReadError = None        
                return ReadError, None
        except HTTPError as ReadError:
            ReadError = None        
            return ReadError, None
        html = req.content.decode(self.encode, 'ignore')
        try :
            url = html.geturl() #Für den Fall redirecting der url
        except: 
            pass
        try :
            soup = BeautifulSoup(html, "lxml")
        except AttributeError as ReadError:
            ReadError = None        
            return ReadError, None
        return url, soup

    def get_links(self, scheme, url):
        pages = []
        page_content = []
        url, soup_object = self.get_object(url)
        if not url :
            return pages, page_content
        domain = urlparse(url).netloc
        log_text = "Website crawlen: " + domain + "\n"
        self.log_daten.append(log_text)
        if len(self.pages) == 0 and len(soup_object.findAll("a")) == 0:
            self.log.append("Keine weiterführenden Links gefunden")
            return False
        for link in soup_object.findAll("a") :
            link_ref = link.get('href')
            ref_text = self.text_splitter(self.clean_text(link.text))
            if not link_ref :
                continue
            parse_not = False
            linktext = link_ref.split(".") + ref_text
            for wort in linktext :
                if wort.lower() in self.no_search_sites :
                    parse_not = True
                    break
            if parse_not :
                continue
            newpage = self.verify_sub_url(scheme, domain, link_ref)
            if newpage and (newpage not in pages):
                pages.append(newpage)
                page_content.append((newpage, soup_object))
        if len(pages) == 0 :
            self.log_daten.append("Keine weiterführenden Links gefunden")
            return [], []
        else :
            return pages, page_content                          
                        
    def verify_sub_url(self, scheme, domain, url):
        urlpage = urlparse(url)
        if urlpage.scheme and not urlpage.scheme.startswith("http") :
            return None
        if urlpage.netloc and urlpage.netloc != domain :
            nl = urlpage.netloc.replace("www.","",1)
            dm = domain.replace("www.","",1)
            if nl != dm :
                return None
        path = urlpage.path
        parse_not = False
        for filetype in self.no_search_filetypes :
            if path and path.lower().endswith(filetype) :
                parse_not = True
                break
        if parse_not :
            return None
        if path and path.startswith("/") :
            path = path.replace("/","",1)
        if path and path.startswith("../") :
            path = path.replace("../","",1)
        if path and path.startswith("./") : #wenn relativer Pfad angegeben wird
            urlpath = urlparse(url).path
            urlpathlist = urlpath.split("/")
            path_location = urlpathlist[:len(urlpathlist)-1]
            pathlist = path.split("/")
            location = pathlist[1:]
            newpath = ""
            for part in path_location :
                if part :
                    if newpath :
                        newpath = newpath + "/" + part
                    else :
                        newpath = part
            newpath = newpath + "/" + location[0]
            path = newpath
        query = urlpage.query
        if query :
            query = "?" + query
              
        newpage = scheme + "//" + domain + "/" + path + query
        self.log_daten.append("Gefunden: " + newpage)
        return newpage

    def verify_main_url(self, url):
        test = urlparse(url)
        if not test.scheme :
            scheme = "http:"
        else :
            scheme = test.scheme
            if not scheme.endswith(":") :
              scheme = scheme + ":"
        if not test.netloc :
             netloc = test.path
             path = ""
        else :
             netloc = test.netloc
        if test.path != netloc:
             path = test.path
        url = scheme + "//" + netloc + "/" + path
        return scheme, url


    def get_page_text(self) :
        page_refs = ["title", "h1", "h2", "h3", "h4", "h5", "p", "li", "div", "br"]
        title_refs = ["title", "h1", "h2", "h3", "h4", "h5"]
        text_set = set()
        textliste = []
        for page in self.page_content :
            url, soup = page
            for ref in page_refs :
                seite = soup.findAll(ref)
                for zeile in seite :
                    ref_text = zeile.get_text()
                    if ref in title_refs :
                        wortliste = self.text_splitter(self.clean_text(ref_text))
                        for wort in wortliste :
                            if wort.lower() in self.no_search_sites :
                                ref_text = ""
                    if ref_text :
                        clean_text = self.clean_text(ref_text)
                        text_set.add(clean_text)
        for seite in text_set :
            clean_seite = self.clean_text(seite)
            textliste.append(clean_seite)
        return textliste

    def text_splitter(self, text) :
        textliste = []
        wortliste = re.split(r"(\W)", text)
        for wort in wortliste :
            if wort.isalnum() and wort.lower() not in self.stopwords:
                textliste.append(wort.lower())
        return textliste
        
    def suchwort_extractor(self):
        pages = []
        for page in self.page_text :
            text = self.text_splitter(page)
            if text :
                pages = pages + text
        fd = FreqDist(pages)
        anzahl = len(fd)
        unigramme = fd.most_common(anzahl)
        df1 = pd.DataFrame(unigramme, columns=["Wort", "Anzahl "])
        pairs = nltk.bigrams(pages)
        fdist = FreqDist(pairs)
        bigramme = fdist.most_common(anzahl)
        df2 = pd.DataFrame(bigramme, columns=["Bigramme", "Anzahl  "])
        triples = nltk.trigrams(pages)
        fdist = FreqDist(triples)
        trigramme = fdist.most_common(anzahl)
        df3 = pd.DataFrame(trigramme, columns=["Trigramme", " Anzahl  "])
        df = pd.concat([df1, df2, df3], axis=1)
        return df

    def clean_text(self, text):
        text = text.replace("Ã¼", "ü")
        text = text.replace("Ã¶", "ö")
        text = text.replace(" Ã", " Ü")
        text = text.replace("Ãb", "Üb")
        text = text.replace("Ã¤", "ä")
        text = text.replace("ß¤", "ä")
        text = text.replace("Ã", "ß")
        return text


"""
print("Crawler gestartet")
KiCrawl = Crawler("www.meierguss.de")
#KiCrawl = Crawler("www.bus-taxi-sieben.de")
#KiCrawl = Crawler("http://www.aundwtiefbau.de")
ok = KiCrawl.crawl_site()
print("Ende des Programms")
"""

