﻿from flask_wtf import Form
from wtforms import StringField, SubmitField, TextAreaField, IntegerField
from wtforms.validators import Required


class AIDForm(Form):
    aid = StringField('AID eingeben (Crefo-Nummer)') #, validators=[Required()])
    suchworte = TextAreaField('Zusätzliche Suchworte eingeben (z.B. aus M-Akte)')
    submit = SubmitField('Submit')

class EntityForm(Form):
    aid = StringField('AID eingeben (Crefo-Nummer)', validators=[Required()])
    submit = SubmitField('Absenden')

class StopwortForm(Form):
    anzahl = IntegerField('Anzahl der Stopworte eingeben', validators=[Required()])
    submit = SubmitField('Absenden')

