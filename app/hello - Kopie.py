from flask import Flask, render_template
from flask import request

app = Flask(__name__)


@app.route('/')
def index():
    return '<p>Hello World - Good evening</p>'


@app.route('/user/<name>')
def user(name):
    return '<h1>Hello, %s. \nHow are you this evening - ok  ?</h1>' % name
    return '<p>Everything ok ?</p>'


if __name__ == '__main__':
    app.run(debug=True)

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

