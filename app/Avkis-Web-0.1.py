# -*- coding: utf-8 -*-

from flask import Flask, render_template, g
from openpyxl.utils import get_column_letter
from flask_script import Manager
from flask_bootstrap import Bootstrap
from pandas import Series, DataFrame, ExcelWriter
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter import simpledialog

import os
import pandas as pd
import shelve
import sqlite3
import openpyxl



globals = {}
globals["dbfile"] = ""
globals["records"] = ""

"""---------------------------------------------------
Funktion:   Datenbank_Oeffnen
Zweck:      Öffnet die SQLite-Datenbank mit den Firmendaten
-----------------------------------------------------"""
def Datenbank_Oeffnen():
    root = Tk()
    root.withdraw()
    options = {}
    options['defaultextension'] = '.db'
    options['filetypes'] = [('db files', '.db'), ('All files', '.*')]
    options['initialdir'] = DBDir
    options['initialfile'] = 'Firmen.db'
    options['parent'] = root
    options['title'] = 'Datenbank wählen'
    database = filedialog.askopenfilename(**options)
    root.destroy()
    return(database)

"""--------
Funktion: My_Dialog
--------"""
def MyDialog(text, msg):
    root = Tk()
    root.withdraw()
    aid = simpledialog.askstring(text, msg)
    root.destroy()
    return aid

"""--------
Funktion: tfidf
--------"""
def tfidf(Database, connection, cursor, Company, selektion=[]):
#    connection = sqlite3.connect("C:\\Users\\Holger\\Dropbox\\Avandil\\AVKIS\\Firmen.db")
#    connection = sqlite3.connect(Database)
#    cursor = connection.cursor()

    raw_data = pd.read_sql("SELECT * from TFIDF",connection)

#    Company = "2010053956"

    for index, row in raw_data.iterrows():
        if row["AID"] == Company:
            Nummer = index
            break

    WZ_Gewichtung = 2

    companies = raw_data["AID"]

    # Bewertung der WZ-Codes

    WZ_Liste = raw_data["BCH"]
    tfidf_vectorizer = TfidfVectorizer()
    tfidf_matrix = tfidf_vectorizer.fit_transform(WZ_Liste)
    ar = cosine_similarity(tfidf_matrix[Nummer:Nummer+1], tfidf_matrix)
    Ergebnisliste = ar[0]
    data = {"Company" : companies, "Wert" : Ergebnisliste}
    frame_WZ = DataFrame(data)
    frame_WZ["Wert"] = frame_WZ["Wert"] * WZ_Gewichtung
#    print("WZ-Codes")
#    print(frame_WZ.sort_values(by="Wert", ascending = False))

    # Bewertung der TÃ¤tigkeitsbeschreibung

    TT_Liste = raw_data["Tätigkeit"] + raw_data["Name"]
    tfidf_vectorizer = TfidfVectorizer()
    tfidf_matrix = tfidf_vectorizer.fit_transform(TT_Liste)
    ar = cosine_similarity(tfidf_matrix[Nummer:Nummer+1], tfidf_matrix)
    Ergebnisliste = ar[0]
    data = {"Company" : companies, "Wert" : Ergebnisliste}
    frame_Ttg = DataFrame(data)
#    print("TÃ¤tigkeitsbeschreibung")
#    print(frame_Ttg.sort_values(by="Wert", ascending = False))

    frame_final = frame_WZ
    frame_final["Wert"] = frame_WZ["Wert"] + frame_Ttg["Wert"]
    frame_final["Name"] = raw_data["Name"]
    frame_final["Tätigkeit"] = raw_data["Tätigkeit"]
    
#    print("Bewertung")
#    print(frame_final.sort_values(by="Wert", ascending = False))

    result_frame = frame_final.sort_values(by="Wert", ascending = False)
    if not selektion :
        result_frame = result_frame[result_frame.Wert > 0.1]
        result_frame = result_frame[result_frame.Wert != 2.0]
    else :
        result_frame = result_frame[result_frame["Company"].isin(selektion)]
    
    Anzahl = result_frame.count()
    Text = "Anzahl der Matches: " + str(Anzahl)
    root = Tk()
    root.withdraw()
    messagebox.showinfo("Ergebnis", Text) 
    root.destroy()
    return(result_frame)


"""---------------------------------------------------
Intitialisierung des Hauptprogramms
-----------------------------------------------------"""
global DBDir, ErrlogFile

environment = "Win"
#environment = "Mac"

if environment == "Win" :
    backslash = "\\"
    shelfFile = shelve.open("avkis_Win")

if environment == "Mac" :
    backslash = "/"
    shelfFile = shelve.open("avkis_Mac")

shelfFile = shelve.open("avkis")

if "DBDir" in shelfFile :
    DBDir = shelfFile["DBDir"]
else :
    DBDir = os.getcwd()
    if environment == "Win" :
        DBDir = "C:\\Lokale Dateien"
    if environment == "Mac" :
        DBDir = "/Volumes/Untitled/AVKIS"

    
    
#ErrlogFile = DBDir + "\\Errlog.txt"
ErrlogFile = DBDir + backslash + "Errlog.txt"


"""---------------------------------------------------
FLASK - Hauptprogramm
-----------------------------------------------------"""

app = Flask(__name__)
manager = Manager(app)
bootstrap = Bootstrap(app)


@app.route('/')
def index():
    dbfile = globals["dbfile"]
    records = globals["records"]
    print("Datenbank: ", dbfile)    
    return render_template('index.html', datenbank=dbfile, records=records)

@app.route('/user/<name>')
def user(name):
    return render_template('user.html', name=name)

@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404

@app.route('/Datenbank_Oeffnen/')
def datenbank_oeffnen():

    globals["dbfile"] = Datenbank_Oeffnen()
    dbfile = globals["dbfile"]
    globals["connection"] = sqlite3.connect(dbfile)
    globals["cursor"] = globals["connection"].cursor()

    cursor = globals["cursor"]    
    cursor.execute("select count(AID) from TFIDF")
    records = cursor.fetchone()
    rec_count = format(records[0],",").replace(",",".")
    globals["records"] = rec_count

    return render_template('index.html', datenbank=dbfile, records=rec_count)

@app.route('/Datenbank_Zeigen/')
def datenbank_zeigen():
    dbfile = globals["dbfile"]
    cursor = globals["cursor"]    
    cursor.execute("SELECT AID, Name, Tätigkeit FROM TFIDF")
    return render_template('show_database.html', items=cursor.fetchall())

@app.route('/KI_Suche/')
def ki_suche():
    dbfile = globals["dbfile"]
    connection = globals["connection"]    
    cursor = globals["cursor"]    
    aid = MyDialog("AID eingeben", "AID:")
    globals["AID"] = aid
    ki_ergebnis = tfidf(dbfile,connection, cursor, aid)
    globals["KI-Ergebnis"] = ki_ergebnis
    return render_template('ki_ergebnis.html', items=ki_ergebnis)

@app.route('/KI_Export/')
def ki_export():
    root = Tk()
    root.withdraw()
    options = {}
    options['defaultextension'] = '.xlsx'
    options['filetypes'] = [('Excel files', '.xlsx'), ('All files', '.*')]
    options['initialdir'] = DBDir
    options['initialfile'] = 'AID.xlsx'
    options['parent'] = root
    options['title'] = 'Datenbank wählen'
    ExportFile = filedialog.asksaveasfilename(**options)
    root.destroy()
    writer = pd.ExcelWriter(ExportFile)
    df1 = globals["KI-Ergebnis"]
    df1.to_excel(writer, sheet_name='KI-Suche', index=False)
    writer.save()
    Text = "Excel-Datei erstellt"
    root = Tk()
    root.withdraw()
    messagebox.showinfo("Ergebnis", Text) 
    root.destroy()
    return render_template('index.html', datenbank=globals["dbfile"], records=globals["records"])

@app.route('/KI_Check/')
def Liste_Pruefen() :
    root = Tk()
    root.withdraw()
    options = {}
    options['defaultextension'] = '.xlsx'
    options['filetypes'] = [('Excel files', '.xlsx'), ('All files', '.*')]
    options['initialdir'] = DBDir
    options['initialfile'] = 'KI-Suche.xlsx'
    options['parent'] = root
    options['title'] = 'Datei wählen'
    ExcelFile = filedialog.askopenfilename(**options)
    root.destroy()
    if not ExcelFile :
        return
    wb = openpyxl.load_workbook(ExcelFile)
    try:
        sheet = wb.get_sheet_by_name("KI-Suche")
    except :
        Text = 'Die Datei\n\n' + ExcelFile + '\n\nenthält kein Sheet mit dem Namen "KI-Suche" !\nVorgang wird abgebrochen.'
        messagebox.showinfo("Excel: Dateifehler", Text) 
    ueberschrift = sheet["A1"].value
    if ueberschrift != "AID" :
        Text = 'Das Excel-Sheet "KI-Suche" enthält keine Überschrift "AID"" !\nVorgang wird abgebrochen.'
        root = Tk()
        root.withdraw()
        messagebox.showinfo("Excel: Dateifehler", Text) 
        root.destroy()
    ki_liste = []
    anzahl = sheet.max_row + 1
    for row in range(2, anzahl):
        aid = sheet["A" + str(row)].value
        ki_liste.append(aid)
    ki_ergebnis = tfidf(globals["dbfile"],globals["connection"], globals["cursor"], ki_liste[0], ki_liste)
    globals["KI-Ergebnis"] = ki_ergebnis
    return render_template('ki_ergebnis.html', items=ki_ergebnis)


if __name__ == '__main__':
    app.run()
