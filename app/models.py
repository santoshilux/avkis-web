# -*- coding: utf-8 -*-
#from flask import current_app
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from . import db
from . import login_manager

class tfidf(db.Model):
    __tablename__ = 'TFIDF'
    AID = db.Column(db.Text, index=True, primary_key=True)
    Name = db.Column(db.Text)
    Tätigkeit = db.Column(db.Text)
    BCH = db.Column(db.Text)
    BCN = db.Column(db.Text)
    Internet = db.Column(db.Text)
    
class adresse(db.Model):
    __tablename__ = 'Adresse'
    AID = db.Column(db.Text, index=True, primary_key=True)
    AD = db.Column(db.Text)
    Adresszusatz = db.Column(db.Text)
    Strasse_Nr = db.Column(db.Text)
    PLZ = db.Column(db.Text)
    Ort = db.Column(db.Text)
    Landkreis = db.Column(db.Text)
    Bundesland = db.Column(db.Text)
    Land = db.Column(db.Text)
    GKZ_X = db.Column(db.Text)
    GKZ_Y = db.Column(db.Text)
    GKZ_Street = db.Column(db.Text)
    Erfassungsdatum = db.Column(db.Text)

class kommunikation(db.Model):
    __tablename__ = 'Kommunikation'

    AID = db.Column(db.Text, index=True, primary_key=True)
    KTyp = db.Column(db.Text)
    KWert = db.Column(db.Text)

class User(UserMixin, db.Model):
    _tablename_ = "User"
    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String(64), unique = True, index = True)
    username = db.Column(db.String(64), unique = True, index = True)
    password_hash = db.Column(db.String(128))
    is_admin = db.Column(db.Integer)

    @property
    def password(self):
        raise AttributeError("Passwort nicht lesbar")
        
    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)
        
    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)
    
class financials(db.Model):
    __tablename__ = 'Financials'
    AID = db.Column(db.Text)
    Jahr = db.Column(db.Text)
    FTyp = db.Column(db.Text)
    FWert = db.Column(db.Text, primary_key=True)

class branchencodes(db.Model):
    __tablename__ = 'Branchencodes'

    AID = db.Column(db.Text)
    Branchencode = db.Column(db.Text, primary_key=True)
    Branchencode_HN = db.Column(db.Text)
    Source = db.Column(db.Text)
    Erfassungsdatum = db.Column(db.Text)

class entity(db.Model):
    __tablename__ = 'Entity'

    ID = db.Column(db.Text, primary_key=True)
    AID = db.Column(db.Text)
    Entity_Name = db.Column(db.Text)
    Entity_Vorname = db.Column(db.Text)
    Eigentumsadresse = db.Column(db.Text)
    EntityTyp = db.Column(db.Text)
    Rechtsform = db.Column(db.Text)
    Anrede = db.Column(db.Text)
    Titel = db.Column(db.Text)
    Ausbildung = db.Column(db.Text)
    Geschlecht = db.Column(db.Text)
    Geburtsjahr = db.Column(db.Text)
    Vergleichsgruppe_Code = db.Column(db.Text)
    Vergleichsgruppe_Size = db.Column(db.Text)
    Anzahl_Gesellschaften = db.Column(db.Text)
    Anzahl_Gruppenunternehmen = db.Column(db.Text)
    Anzahl_Tochtergesellschaften = db.Column(db.Text)
    Handelsregister = db.Column(db.Text)
    Handelsregister_Nummer = db.Column(db.Text)
    UID = db.Column(db.Text)
    Erfassungsdatum = db.Column(db.Text)
    Reserviert = db.Column(db.Text)
    Ablaufdatum = db.Column(db.Text)

class role(db.Model):
    __tablename__ = 'Role'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)

class tatigkeit(db.Model):
    __tablename__ = 'Tätigkeit'

    AID = db.Column(db.Text)
    Tätigkeitsbeschreibung = db.Column(db.Text, primary_key=True)

class websites(db.Model):
    __tablename__ = 'Websites'

    AID = db.Column(db.Text, primary_key=True)
    Content = db.Column(db.Text)
    Weblink = db.Column(db.Text)

class web_search_result(db.Model):

    __tablename__ = 'web_search_result'

    Id = db.Column(db.Integer, primary_key=True)
    daturn = db.Column(db.Text)
    chiffre = db.Column(db.Text)
    beschreibung = db.Column(db.Text)
    standorte = db.Column(db.Text)
    anzahl_mitarbeiter = db.Column(db.Text)
    letzter_jahresumsatz = db.Column(db.Text)
    Preisvorstellung = db.Column(db.Text)
    tatigteit = db.Column(db.Text)
    last_crawl_data = db.Column(db.Text)
    reference_url = db.Column(db.Text)

    def __init__(self,daturn,chiffre,branchen,beschreibung,standorte,anzahl_mitarbeiter,letzter_jahresumsatz, \
                 preisvorstellung,tatigkeit):
        self.daturn = daturn
        self.chiffre = chiffre
        self.branchen = branchen
        self.beschreibung = beschreibung
        self.standorte = standorte
        self.anzahl_mitarbeiter = anzahl_mitarbeiter
        self.letzter_jahresumsatz = letzter_jahresumsatz
        self.Preisvorstellung = preisvorstellung
        self.tatigteit = tatigkeit

class web_branch_data(db.Model):

    __tablename__ = 'web_branch_data'

    Id = db.Column(db.Integer, primary_key=True)
    chiffre = db.Column(db.Text)
    sektor = db.Column(db.Text)
    rubrik = db.Column(db.Text)
    einzelbranche = db.Column(db.Text)

class web_location_data(db.Model):

    __tablename__ = 'web_location_data'

    Id = db.Column(db.Integer, primary_key=True)
    chiffre = db.Column(db.Text)
    bundesland = db.Column(db.Text)
    region = db.Column(db.Text)
    kreis = db.Column(db.Text)

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class web_data_storage(db.Model):

    __tablename__ = 'web_data_storage'

    Id = db.Column(db.Integer, primary_key=True)
    branchcode = db.Column(db.Text)
    langtitel = db.Column(db.Text)
    einschlusse = db.Column(db.Text)
    ausschlusse = db.Column(db.Text)