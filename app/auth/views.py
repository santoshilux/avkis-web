# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 18:34:01 2017

@author: Holger
"""

from flask import render_template, redirect, request, url_for, flash, abort, current_app, session
from flask_login import login_user, logout_user, login_required, current_user
from urllib.parse import urlparse, urljoin
from . import auth
from .. import db
from ..models import User
from .forms import LoginForm, RegistrationForm
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import pdb, json

#Debug: pdb.set_trace()

"""--------
Function: is_safe_url
--------"""
def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

"""--------
Function: auth.route
--------"""
@auth.route('/login', methods=['GET', 'POST'])
def login():
    # print("---------------111111111111111111111111111111111111")
    # print(current_app.config['DATABASE'])
    form = LoginForm()
    language = session.get('lang','german')
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        print("Admin: ", user.is_admin)
        if user.is_admin == 1:
                current_app.config["IS_ADMIN"] = True
                print("IS_ADMIN: ", current_app.config["IS_ADMIN"])
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            flash("Anmeldung akzeptiert")
            next = request.args.get("next")
            if not is_safe_url(next):
                return abort(404)
            session['lang'] = language
            return redirect(request.args.get(next) or url_for("main.index", is_admin=current_app.config["IS_ADMIN"],language=language))
        flash("Ungültiger Benutzer / Falsches Passwort")
    session['lang'] = language
    print(session)
    return render_template("auth/login.html", form=form,language=language)

@auth.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    print("gjhdbasbkdasndnsadnkdsnkdnkasdkdsakndkasnka")
    current_app.config["IS_ADMIN"] = True
    logout_user()
    flash("Sie sind abgemeldet")
    return redirect(url_for("main.index", is_admin=False))

 
@auth.route('/register', methods=['GET', 'POST'])
@login_required
def register():
    form = RegistrationForm()
    language = session.get('lang', 'german')
    if form.validate_on_submit():
        user = User(email=form.email.data, username=form.username.data, password=form.password.data, is_admin=form.is_admin.data)
        db.session.add(user)
        flash("Nutzer wurde registriert")
    return render_template("./auth/register.html", form=form, language=language)

@auth.route('/change_password/',  methods = ["GET", "POST"])
@login_required
def change_password() :
    passw = request.args.get('pass','')
    form = RegistrationForm()
    change_pass = request.args.get('change','')
    if change_pass:
        return render_template('change_password.html', form=form, type="change_pwd")
    elif passw:
        user = User.query.filter_by(email=current_user.email).first()
        user.password = passw
        db.session.commit()
        flash("Password changed successfully")
        return render_template('change_password.html', form=form)
    else:
        to = "sntshkumar730830@gmail.com"
        froms = "santosh.erpincloud@gmail.com"
        smtpserver = smtplib.SMTP("smtp.gmail.com", 587)
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Link to change password"
        msg['From'] = froms
        msg['To'] = to
        html = "http://localhost:5000/auth/change_password?change=yes"
        part2 = MIMEText(html, 'html')
        gmail_pwd = "$0lutions"
        msg.attach(part2)
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.ehlo
        smtpserver.login(froms,gmail_pwd)
        smtpserver.sendmail(froms,to,msg.as_string())
        smtpserver.close()
        return render_template('change_password.html',form=form, type="change_pwd_link")