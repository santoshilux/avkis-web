# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 18:31:39 2017

@author: Holger
"""

from flask import Blueprint

auth = Blueprint("auth", __name__)

from . import views
