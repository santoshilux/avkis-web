# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 18:55:14 2017

@author: Holger
"""

from flask_wtf import FlaskForm as Form
from wtforms import StringField, SubmitField, PasswordField, BooleanField
from wtforms.validators import Required, Email, Length, Regexp, EqualTo
from wtforms import ValidationError
from ..models import User


class LoginForm(Form):
    email = StringField("Email", validators=[Required(), Length(1,64), Email()])
    password = PasswordField("Passwort", validators=[Required()])
    remember_me = BooleanField("Eingeloggt bleiben")
    submit = SubmitField("Log In")
    
class RegistrationForm(Form):
    email = StringField("Email", validators=[Required(), Length(1,64), Email()])
    username = StringField("Name", validators=[Required(), Length(1, 64), Regexp("^[A-Za-z0-9_.]*$", 0, 
                                               "Namen dürfen nur Buchstaben, Ziffern, Punkt oder Unterstrich enthalten")])
    password = PasswordField("Passwort", validators=[Required(), EqualTo("password2", message="Passwörter müssen übereinstimmen")])
    password2 = PasswordField("Passwort bestätigen", validators=[Required()])
    is_admin = BooleanField("Administrator")
    submit = SubmitField("Anlegen")
    
    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email bereits vorhanden")
            
    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError("Name bereits vorhanden")
        